#!/bin/sh
# Copyright (C) 2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

set -e

VERSION=$(cat VERSION)
FILENAME="python-asterisk-api_${VERSION}.orig.tar.gz"
URL="https://gitlab.com/xivo.solutions/ari-py/repository/archive.tar.gz?ref=${VERSION}"

rm -rf tmp
mkdir tmp
cd tmp

wget -nv -O "${FILENAME}" "${URL}"
tar xf "${FILENAME}"
mv ari-py-${VERSION}-* ari-py-${VERSION}

cd ..
rsync -av "tmp/ari-py-${VERSION}/" .
