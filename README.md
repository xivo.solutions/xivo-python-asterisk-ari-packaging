# The packaging information for python asterisk ari in XiVO

This repository contains the packaging information for
[python-ari](https://github.com/asterisk/ari-py).

To get a new version of python-ari in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
